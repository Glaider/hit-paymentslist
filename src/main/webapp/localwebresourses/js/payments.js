var hit = {};
hit.hostName = "http://localhost:8080";
//noinspection JSUnresolvedVariable
google.charts.load('current', {'packages': ['bar', 'corechart']});

//Draw Chosen Chart
hit.drawChart = function (chartType) {
    $('#chartContent').empty();
    $('#chartModalMessage').empty();
    $.ajax({
        type: "POST",
        url: hit.hostName + '/Controller/CurrentMonthLimits',
        dataType: 'json',
        async: true,
        data: {
            monthID: hit.user.currMonthID
        },
        success: function (data) {
            if (data.getData == 'success') {

                switch (chartType) {
                    case 'pieChartAction':
                        hit.createPieCharts(data);
                        break;
                    case 'barChartAction':
                        hit.createBarChart(data);
                        break;
                    default:
                        break;
                }
            }
            else if (data.getData == 'fail') {
                $('#chartModalMessage').load("ReplacesContent.html #failToLoadChart");
            }
        },
        error: function () {
            $('#chartModalMessage').load("ReplacesContent.html #failToLoadChart");
        }
    });
};


//Create Bar Chart
hit.createBarChart = function (limitsData) {

    hit.barChart.isPipeChartLoaded = true;
    hit.pipeCharts.isPieChartsLoaded = false;
    var options = {
        title: 'Month Payment Limits Bar',
        backgroundColor: {fill: 'F0F0F0'}
    };

    //noinspection JSUnresolvedVariable,JSUnresolvedFunction,JSPotentiallyInvalidConstructorUsage
    var data = new google.visualization.arrayToDataTable([
        ['Spent Limits', 'Limit', 'Spent'],
        ['Current Month', limitsData.monthLimit, limitsData.monthLimitSpent],
        ['Current Week', limitsData.weekLimit, limitsData.weekLimitSpent],
        ['Current Day', limitsData.dayLimit, limitsData.dayLimitSpent]
    ]);


    //noinspection JSUnresolvedVariable,JSUnresolvedFunction
    var chart = new google.charts.Bar(document.getElementById('chartContent'));
    //noinspection JSUnresolvedFunction,JSUnresolvedFunction,JSUnresolvedVariable
    chart.draw(data, google.charts.Bar.convertOptions(options));
};

//Create Pipe Charts
hit.createPieCharts = function (limitsData) {
    hit.pipeCharts.isPieChartsLoaded = true;
    hit.barChart.isPipeChartLoaded = false;


    //noinspection JSUnresolvedVariable,JSUnresolvedFunction
    var dataLimits = google.visualization.arrayToDataTable([
        ['Period', 'Amount'],
        ['Month', limitsData.monthLimit],
        ['Week', limitsData.weekLimit],
        ['Day', limitsData.dayLimit]
    ]);

    var limitsPieOptions = {
        title: 'Limits Pie',
        is3D: true,
        backgroundColor: {fill: 'F0F0F0'}
    };

    //noinspection JSUnresolvedVariable,JSUnresolvedFunction
    var dataSpent = google.visualization.arrayToDataTable([
        ['Period', 'Amount'],
        ['Month', limitsData.monthLimitSpent],
        ['Week', limitsData.weekLimitSpent],
        ['Day', limitsData.dayLimitSpent]
    ]);


    var spentPieOptions = {
        title: 'Spent Pie',
        is3D: true,
        backgroundColor: {fill: 'F0F0F0'}
    };


    var chartsTable = $('<table class="columns" style="width: 100%"><tr class="pieChartRow"><td ><div id="firstPie" style="border: 1px solid #000000"></div></td></tr><tr class="pieChartRow"><td ><div id="secondPie" style="border: 1px solid #000000"></div></td></tr></table>');
    $('#chartContent').append(chartsTable);


    //noinspection JSUnresolvedVariable,JSUnresolvedFunction
    var firstChart = new google.visualization.PieChart(document.getElementById('firstPie'));
    //noinspection JSUnresolvedFunction
    firstChart.draw(dataLimits, limitsPieOptions);

    //noinspection JSUnresolvedVariable,JSUnresolvedFunction
    var secondChart = new google.visualization.PieChart(document.getElementById('secondPie'));
    //noinspection JSUnresolvedFunction
    secondChart.draw(dataSpent, spentPieOptions);
};

$(function () {

    $(document).ready(function () {
            $('#logInOut').load("ReplacesContent.html #logInNavBarBTN >*");
            $('#collapsedBTN').load("ReplacesContent.html #collapseLogInButton >*");
            $('#sideBarMenu').hide();
            $('#wrapper').css('padding-left', 0);
            $('#aboutAppDiv').load("ReplacesContent.html #aboutAppContent >*");
            hit.isLogetIn = false;
            hit.fetchAboutDiv();
    });

    //Configure size of about div
    hit.fetchAboutDiv = function () {
        var navBarDefault = $('.navbar-default');
        var aboutAppDiv = $('#aboutAppDiv');
        var vph = $(document.body).height() - navBarDefault.height() - parseInt(navBarDefault.css('marginBottom')) - 50;
        aboutAppDiv.css('height', vph);
        aboutAppDiv.css('width', '100%');
    };


    window.onresize = function () {
        if (hit.isLogetIn === false) {
            hit.fetchAboutDiv();
        }
    };

    //New Day Start Method
    hit.newDayStart = function () {
        $('#dayChangeModalMessage').append("<div class='alert alert-info'><strong>New Day Started!</strong>Please Re login.</div>");
        hit.logoutAjaxCall('user');
        $('#dayChangeModal').modal('show');
    };

    //On hide Day Change Modal Event
    $('#dayChangeModal').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $('#dayChangeModalMessage').empty();
    });

    //User Logout BTN click Event
    $(document).on("click", "#logOutBTN", function (e) {
        e.preventDefault();
        hit.logoutAjaxCall('user');
    });

    //Admin Logout BTN click Event
    $(document).on("click", ".adminLogoutBTN", function (e) {
        e.preventDefault();
        hit.logoutAjaxCall('admin');
    });

    //Logout Request
    hit.logoutAjaxCall = function (logoutType) {
        $.ajax({ // ajax call starts
            type: "POST",
            url: hit.hostName + '/Controller/Logout',
            async: true,
            success: function () {
                hit.logOutByType(logoutType);
            },
            error: function () {
                $('#dangerPopUpModalMessage').append('<p>There have been some problems when exiting the application.</p><p>We are suggesting you to reload the application!</p>');
                $('#dangerPopUpModal').modal('show');
                hit.logOutByType(logoutType);
            }
        });
    };

    //Call Logout Method By type
    hit.logOutByType = function (logoutType) {
        switch (logoutType) {
            case 'user':
            {
                hit.userLogout();
                break;
            }
            case 'admin':
            {
                hit.adminLogOut();
                break;
            }
            default:
                break;
        }
    };

    //User Logout Method
    hit.userLogout = function () {
        var payTable = $('#payTable');
        var wrapper = $('#wrapper');
        var logInOut = $("#logInOut");
        var collapsedBTN = $("#collapsedBTN");
        payTable.hide();
        payTable.empty();
        logInOut.empty();
        if (wrapper.hasClass('toggled')) {
            wrapper.toggleClass("toggled");
        }
        logInOut.load("ReplacesContent.html #logInNavBarBTN >*");
        collapsedBTN.empty();
        collapsedBTN.load("ReplacesContent.html #collapseLogInButton >*");
        $("#sideBarMenu").hide();
        clearInterval(hit.interval);
        wrapper.css('padding-left', 0);
        $('#aboutAppDiv').load("ReplacesContent.html #aboutAppContent >*");
        hit.isLogetIn = false;
        hit.fetchAboutDiv();
        $("#helloUserName").text("My Payments Table");
    };

    //Admin Logout method
    hit.adminLogOut = function () {
        var logInOut = $("#logInOut");
        var collapsedBTN = $("#collapsedBTN");
        $("#adminDataList").empty();
        logInOut.empty();
        logInOut.load("ReplacesContent.html #logInNavBarBTN >*");
        collapsedBTN.empty();
        collapsedBTN.load("ReplacesContent.html #collapseLogInButton >*");
        clearInterval(hit.adminLogInterval);
        hit.isLogetIn = false;
        $('#aboutAppDiv').load("ReplacesContent.html #aboutAppContent >*");
        hit.fetchAboutDiv();
        $("#helloUserName").text("My Payments Table");
    };

    //Fetch Received User Data
    hit.fetchUserData = function (JSONObject) {
        hit.user = {};
        hit.user.userName = JSONObject.userName;
        hit.user.paymentLimitsList = JSONObject.paymentLimitsList;
        hit.CurrYear = new Date().getFullYear();
        hit.CurrMonth = new Date().getMonth() + 1;
        var logInOut = $("#logInOut");
        var collapsedBTN = $("#collapsedBTN");
        logInOut.empty();
        logInOut.load("ReplacesContent.html #standardSideMenuBTN>*");
        collapsedBTN.empty();
        collapsedBTN.load("ReplacesContent.html #collapseSideMenuBTN>*");
        $("#sideBarMenu").show();
        $("#helloUserName").text("Hello " + hit.user.userName);
        hit.user.paymentLimitsList.forEach(function (paymentMonth) {
            //noinspection JSUnresolvedVariable
            var date = new Date(paymentMonth.PaymentsMonthData.creationMonthDate);
            if (hit.CurrMonth == date.getMonth() + 1 && hit.CurrYear == date.getFullYear()) {
                //noinspection JSUnresolvedVariable
                hit.fetchCurrentMonthData(paymentMonth.PaymentsMonthData);
                return true;
            }
        });
    };


    //Check Current Received Payment Limits
    hit.isPaymentsNotOverflow = function () {
        var isOverflow = false;
        var dangerPopUpModalMessage = $('#dangerPopUpModalMessage');
        var rwdTable = $('.rwd-table');
        if (parseFloat(document.getElementById('currMonthMaxLimit').value) < parseFloat(document.getElementById('currMonthSpentLimit').value)) {
            dangerPopUpModalMessage.append('<p>You exceeded your Monthly limit!</p>');
            isOverflow = true;
        }
        if (parseFloat(document.getElementById('currWeekMaxLimit').value) < parseFloat(document.getElementById('currWeekSpentLimit').value)) {
            dangerPopUpModalMessage.append('<p>You exceeded your Weekly limit!</p>');
            isOverflow = true;
        }
        if (parseFloat(document.getElementById('currDayMaxLimit').value) < parseFloat(document.getElementById('currDaySpentLimit').value)) {
            dangerPopUpModalMessage.append('<p>You exceeded your Daily limit!</p>');
            isOverflow = true;
        }
        if (isOverflow) {
            rwdTable.css('background', 'rgba(221, 0, 21, 0.62)');
        }
        else {
            rwdTable.css('background', 'rgba(123, 123, 123, 0.5)');
        }
        return isOverflow;
    };

    //Fetch Current Mont Data
    hit.fetchCurrentMonthData = function (monthData) {
        var table = $("#payTable");
        hit.user.currMonthID = monthData.paymentID;
        $('.monthDataID').val(monthData.paymentID);
        hit.fetchLimits(monthData);
        table.append("<thead><th>Title</th><th>Amount</th><th>Description</th><th>Date</th><th> </th></thead>");
        //noinspection JSUnresolvedVariable
        if (monthData.paymentList != null) {
            //noinspection JSUnresolvedVariable
            monthData.paymentList.forEach(function (payment) {
                //noinspection JSUnresolvedVariable
                hit.addTableRow(payment.Payment, table);
            });
        }
        table.fadeIn(2000);
    };

    //Fetch Received Payments Limits
    hit.fetchLimits = function (monthData) {
        //noinspection JSUnresolvedVariable
        $('#currMonthMaxLimit').val(parseFloat(monthData.monthLimit));
        //noinspection JSUnresolvedVariable
        $('#currMonthSpentLimit').val(parseFloat(monthData.monthLimitSpent));
        //noinspection JSUnresolvedVariable
        $('#currWeekMaxLimit').val(parseFloat(monthData.weekLimit));
        //noinspection JSUnresolvedVariable
        $('#currWeekSpentLimit').val(parseFloat(monthData.weekLimitSpent));
        //noinspection JSUnresolvedVariable
        $('#currDayMaxLimit').val(parseFloat(monthData.dayLimit));
        //noinspection JSUnresolvedVariable
        $('#currDaySpentLimit').val(parseFloat(monthData.dayLimitSpent));
    };


    //Add new row to Payments Table
    hit.addTableRow = function (payment, table) {
        //noinspection JSUnresolvedVariable
        var currentPaymentDate = new Date(payment.creationDate);
        var deleteBTN = $("<div class='tableBTN btn-group btn-group-xs paymentDeleteConfirmBack' role='group'><button type='button' class='dpBTN  btn fa fa-trash-o deletePaymentBTN'></button></div>");
        var updateBTN = $("<div class='tableBTN btn-group btn-group-xs' role='group' ><button type='button'  data-toggle='modal' data-target='#editModal' class='dpBTN  btn fa fa-pencil-square-o editPaymentBTN'></button></div>");
        var tr = $('<tr>');
        tr.attr("id", "payment_" + payment.paymentID);
        var tdPaymentID = $('<td data-th="ID" style="display: none">').attr("id", "tdPaymentID_" + payment.paymentID);
        tdPaymentID.text(payment.paymentID);
        var tdPaymentTitle = $('<td data-th="Title">').attr("id", "tdPaymentTitle_" + payment.paymentID);
        tdPaymentTitle.text(payment.header);
        var tdPaymentAmount = $('<td data-th="Amount">').attr("id", "tdPaymentAmount_" + payment.paymentID);
        tdPaymentAmount.text(payment.amount);
        var tdPaymentDescription = $('<td data-th="Description" class="textTableData">').attr("id", "tdPaymentDescription_" + payment.paymentID);
        tdPaymentDescription.text(payment.description);
        var tdPaymentDate = $('<td data-th="Date">').attr("id", "tdPaymentDate_" + payment.paymentID);
        tdPaymentDate.text(currentPaymentDate.getDate() + "/" + (parseInt(currentPaymentDate.getMonth()) + 1) + "/" + currentPaymentDate.getFullYear() + "  " + currentPaymentDate.getHours() + ":" + currentPaymentDate.getMinutes() + ":" + currentPaymentDate.getSeconds());
        tr.append(tdPaymentID, tdPaymentTitle, tdPaymentAmount, tdPaymentDescription, tdPaymentDate,
            $('<td data-th=" ">').append(updateBTN, deleteBTN));
        table.append(tr);
    };


    //Delete Payment BTN Click Event
    $(document).on("click", ".deletePaymentBTN", function (e) {
        e.preventDefault();
        if (hit.tempBTN != null) {
            hit.tempBTNParent.html("");
            hit.tempBTNParent.html(hit.tempBTN);
        }
        hit.tempBTN = $(this);
        hit.tempBTNParent = $(this).parent();
        hit.tempBTNParent.html("");
        hit.tempBTNParent.load("ReplacesContent.html #replaceableButtonsConfirmDelete>*");
    });

    //Refuse Delete Payment BTN click Event
    $(document).on("click", ".refuseDelete", function (e) {
        e.preventDefault();
        var btnParent = $(this).parent();
        btnParent.html("");
        btnParent.html(hit.tempBTN);
        hit.tempBTN = null;
    });


    //Confirm Delete Payment BTN click Event
    $(document).on('click', '.confirmDelete', function (e) {
        e.preventDefault();
        var id = $(this.parentNode).closest('tr').find("td:first").text();
        var value = $(this.parentNode).closest('tr').find("td:nth-child(3)").text();
        var btnParent = $(this).parent();
        btnParent.html("");
        btnParent.html(hit.tempBTN);
        hit.tempBTN = null;
        $.ajax({ // ajax call starts
            type: "POST",
            url: hit.hostName + '/Controller/RemovePayment',
            async: true,
            dataType: 'json',
            data: {paymentID: id, timezone: new Date().getTimezoneOffset()},
            success: function (data) {
                //noinspection JSUnresolvedVariable
                var deletePaymentStatus = data.deletePayment;
                if (deletePaymentStatus == 'success') {
                    //noinspection JSUnresolvedVariable
                    hit.updateSpentLimits(value, data.paymentDate, 'remove');
                    hit.deletePaymentInUI(id);
                    hit.ifNeedToShowOverflowModal();
                }
                else if (deletePaymentStatus == 'fail') {
                    $('#dangerPopUpModalMessage').load("ReplacesContent.html #failToDeletePayment");
                    $('#dangerPopUpModal').modal('show');
                }
            },
            error: function () {
                $('#dangerPopUpModalMessage').load("ReplacesContent.html #failToDeletePayment");
                $('#dangerPopUpModal').modal('show');
            }
        });
    });

    //Remove Deleted Payment From User Interface
    hit.deletePaymentInUI = function (paymentID) {
        var rowID = "payment_" + paymentID;
        var row = document.getElementById(rowID);
        $(row).hide(1000);
        $(row).remove();
    };

    //On show Edit Payment Modal Event
    //noinspection JSJQueryEfficiency
    $('#editModal').on('shown.bs.modal', function (e) {
        var sourceElement = e.relatedTarget;
        var id = $(sourceElement).closest('tr').find("td:first").text();
        var header = $(sourceElement).closest('tr').find("td:nth-child(2)").text();
        var amount = $(sourceElement).closest('tr').find("td:nth-child(3)").text();
        var description = $(sourceElement).closest('tr').find("td:nth-child(4)").text();
        var num = parseFloat(amount);
        hit.editPaymentCurrentValue = num;
        var editModal = $('#editModal');
        editModal.find('#paymentID').val(id);
        editModal.find('#editPaymentAmount').val(num);
        editModal.find('#paymentTitle').val(header);
        editModal.find('#paymentDescription').val(description);
    });


    //On hide Edit Payment Modal Event
    //noinspection JSJQueryEfficiency
    $('#editModal').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        hit.ifNeedToShowOverflowModal();
    });

    //Check If Current Limits Not Overflow
    hit.ifNeedToShowOverflowModal = function () {
        if (hit.isPaymentsNotOverflow()) {
            $('#dangerPopUpModal').modal('show');
        }
    };

    //Edit Payment submit Event
    $('#editForm').submit(function (e) {
        e.preventDefault();
        var paymentID = document.getElementById("paymentID").value;
        var paymentTitle = document.getElementById("paymentTitle").value;
        var paymentAmount = document.getElementById("editPaymentAmount").value;
        var paymentDescription = document.getElementById("paymentDescription").value;
        if ($("#tdPaymentTitle_" + paymentID).value != paymentTitle ||
            $("#tdPaymentAmount_" + paymentID).value != paymentAmount ||
            $("#tdPaymentDescription_" + paymentID).value != paymentDescription
        ) {
            $.ajax({ // ajax call starts
                type: "POST",
                url: hit.hostName + '/Controller/EditPayment',
                dataType: 'json',
                async: true,
                data: {
                    paymentID: paymentID,
                    paymentTitle: paymentTitle,
                    paymentAmount: paymentAmount,
                    paymentDescription: paymentDescription,
                    timezone: new Date().getTimezoneOffset()
                },
                success: function (data) {
                    //noinspection JSUnresolvedVariable
                    var editPaymentStatus = data.editPayment;
                    if (editPaymentStatus == 'success') {
                        $("#tdPaymentTitle_" + paymentID).text(paymentTitle);
                        $("#tdPaymentAmount_" + paymentID).text(paymentAmount);
                        $("#tdPaymentDescription_" + paymentID).text(paymentDescription);
                        //noinspection JSUnresolvedVariable
                        var paymentDate = data.payment.creationDate;
                        hit.updateSpentLimits(hit.editPaymentCurrentValue, paymentDate, 'remove');
                        hit.updateSpentLimits(paymentAmount, paymentDate, 'add');
                        $('#editModal').modal('hide');
                    } else if (editPaymentStatus == 'fail') {
                        $('#editModalMessage').load("ReplacesContent.html #failToEditPayment")
                    }
                },
                error: function () {
                    $('#editModalMessage').load("ReplacesContent.html #failToEditPayment")
                }
            });
        }
    });


    //On hide Danger Message Event
    $('#dangerPopUpModal').on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $('#dangerPopUpModalMessage').html('');
    });


    //Register User submit Event
    $('#registerForm').submit(function (e) {
        e.preventDefault();
        var username = document.getElementById("newUserName").value;
        var password = document.getElementById("newUserPassword").value;
        var email = document.getElementById("newUserEmail").value;
        $.ajax({ // ajax call starts
            type: "POST",
            url: hit.hostName + '/Controller/RegisterNewUser',
            dataType: 'json',
            async: true,
            data: {
                username: username,
                password: password,
                email: email
            },
            success: function (data) {
                //noinspection JSUnresolvedVariable
                var registerStatus = data.register;
                if (registerStatus == "success") {
                    $('#registerMessage').load("ReplacesContent.html #successRegister");
                    $('#registerForm').hide();
                }
                else if (registerStatus == "fail") {
                    $('#registerMessage').load("ReplacesContent.html #failToRegister");
                }
            },
            error: function () {
                $('#registerMessage').load("ReplacesContent.html #failToRegister");
            }
        });
    });

    //Calculate Milliseconds Until Midnight
    hit.getMillisecondsUntilMidnight = function () {
        var midnight = new Date();
        midnight.setHours(24);
        midnight.setMinutes(0);
        midnight.setSeconds(0);
        midnight.setMilliseconds(0);
        return midnight.getTime() - new Date().getTime();
    };

    //Log In submit Event
    $(document).on('submit', ".logInForm", function (e) {
        e.preventDefault();
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        var isRememberUser = $('#rememberMeCheckBox').is(":checked");
        $("#logInBTN").prop("disabled", true);
        $.ajax({ // ajax call starts
            type: "POST",
            url: hit.hostName + '/Controller/LogIn',
            dataType: 'json',
            async: true,
            data: {
                username: username,
                password: password,
                currDateTime: Date.now(),
                timezone: new Date().getTimezoneOffset(),
                isRememberUser: isRememberUser
            },
            success: function (data) {
                var aboutAppDiv = $('#aboutAppDiv');
                //noinspection JSUnresolvedVariable
                var loginStatus = data.loginStatus;
                if (loginStatus == "User") {
                    hit.isLogetIn = true;
                    aboutAppDiv.empty();
                    aboutAppDiv.css('height', 0);
                    aboutAppDiv.css('width', 0);
                    $('#wrapper').css('padding-left', "");
                    hit.barChart = {};
                    hit.barChart.isPipeChartLoaded = false;
                    hit.pipeCharts = {};
                    hit.pipeCharts.isPieChartsLoaded = false;
                    hit.interval = setInterval(hit.newDayStart, hit.getMillisecondsUntilMidnight());
                    //noinspection JSUnresolvedVariable
                    hit.fetchUserData(data.userData);
                    $('#logInModal').modal('hide');
                    $("#rememberMeCheckBox").prop("checked", false);
                    hit.ifNeedToShowOverflowModal();
                }
                else if (loginStatus == "Admin") {
                    var logInOut = $("#logInOut");
                    var collapsedBTN = $("#collapsedBTN");
                    hit.isLogetIn = true;
                    hit.registeredUsers = {};
                    aboutAppDiv.empty();
                    aboutAppDiv.css('height', 0);
                    aboutAppDiv.css('width', 0);
                    logInOut.empty();
                    $('#wrapper').css('padding-left', 0);
                    logInOut.load("ReplacesContent.html #standardAdminLogOutBTN>*");
                    collapsedBTN.empty();
                    collapsedBTN.load("ReplacesContent.html #collapseAdminLogOutBTN>*");
                    //noinspection JSUnresolvedVariable
                    $("#helloUserName").text("Hello " + data.loginStatus);
                    var logLI = $("<li class = 'list-group-item'></li>");
                    $(logLI).append(" <label for='activeSessionsLog' id='activeSessionsLogLabel'>Active Sessions : </label><div class='userSessionsDiv' id='activeSessionsLog'>0</div>");
                    $(logLI).append("<br><label for='maxActiveSessionsLog' id='maxActiveSessionsLogLabel'>Max sessions During Current Observation : </label><div class='userSessionsDiv' id='maxActiveSessionsLog'>0</div>");
                    $(logLI).append("<button type='button' class='btn btn-primary btn-block' id='showRegisteredUsersBTN'>Show Registered Users</button>");
                    $('#adminDataList').append(logLI);

                    $('#logInModal').modal('hide');
                    hit.refetchAdminLogData();
                    hit.adminLogInterval = setInterval(hit.refetchAdminLogData, 5000);
                }
                else if (loginStatus == "Fail") {
                    $("#logInBTN").prop("disabled", false);
                    $("#loginMsg").load("ReplacesContent.html #failLogin");
                }
            },
            error: function () {
                $("#logInBTN").prop("disabled", false);
                $("#loginMsg").load("ReplacesContent.html #failLogin");
            }
        });
    });

    //Show Registered Users To Admin BTN click Event
    $(document).on('click', '#showRegisteredUsersBTN', function (e) {
        e.preventDefault();
        $('#registeredUsersLI').remove();
        $("#showRegisteredUsersBTN").prop("disabled", true);
        $.ajax({ // ajax call starts
            type: "POST",
            url: hit.hostName + '/Controller/Admin',
            dataType: 'json',
            async: true,
            data: {adminDataRequestType: "getRegisteredUsers"},
            success: function (data) {
                //noinspection JSUnresolvedVariable
                var adminDataRequestStatus = data.adminDataRequestStatus;
                $("#showRegisteredUsersBTN").prop("disabled", false);

                if (adminDataRequestStatus == "success") {
                    var newAdminDataLI = $("<li class = 'list-group-item' id='registeredUsersLI'></li>");
                    var registeredUsersTable = $("<table id='adminUsersInfoTable'><thead><td>Username</td><td>Email</td></thead></table>");
                    //noinspection JSUnresolvedVariable
                    for (var registeredUser in data.registeredUsersInfo) {
                        //noinspection JSUnresolvedVariable
                        if (data.registeredUsersInfo.hasOwnProperty(registeredUser)) {
                            //noinspection JSUnresolvedVariable
                            var tableRow = $("<tr><td>" + registeredUser + "</td><td>" + data.registeredUsersInfo[registeredUser] + "</td></tr>");
                            $(registeredUsersTable).append(tableRow);
                        }
                    }
                    $(newAdminDataLI).append(registeredUsersTable);
                    $('#adminDataList').append(newAdminDataLI);
                }
                else if (adminDataRequestStatus == "fail") {
                    alert("Error Connection Problem !!!");
                    hit.adminLogOut();
                }
            },
            error: function () {
                $("#showRegisteredUsersBTN").prop("disabled", false);
                alert("Error Connection Problem !!!");
                hit.adminLogOut();
            }
        });
    });


    //Refetch Admin Log Data
    hit.refetchAdminLogData = function () {
        $.ajax({ // ajax call starts
            type: "POST",
            url: hit.hostName + '/Controller/Admin',
            dataType: 'json',
            async: true,
            data: {adminDataRequestType: "getActiveSessions"},
            success: function (data) {
                //noinspection JSUnresolvedVariable
                var adminDataRequestStatus = data.adminDataRequestStatus;
                if (adminDataRequestStatus == "success") {
                    var maxActiveSessionsLog = $('#maxActiveSessionsLog');
                    //noinspection JSUnresolvedVariable
                    var activeSessions = parseInt(data.activeSessions, 10);

                    $('#activeSessionsLog').text(activeSessions);
                    if (activeSessions > parseInt(maxActiveSessionsLog.text(), 10)) {
                        maxActiveSessionsLog.text(activeSessions);
                    }
                } else if (adminDataRequestStatus == "fail") {
                    alert("Error Connection Problem !!!");
                    hit.adminLogOut();
                }
            },
            error: function () {
                alert("Error Connection Problem !!!");
                hit.adminLogOut();
            }
        });
    };

    //On hide Login Modal Event
    //noinspection JSJQueryEfficiency
    $("#logInModal").on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $("#logInBTN").prop("disabled", false);
        $("#loginMsg").empty();
        $(".loginModalValue").val("");
    });

    //On show Login Modal Event
    //noinspection JSJQueryEfficiency
    $("#logInModal").on('show.bs.modal', function () {
        var username = hit.getCookie('userName');
        if (username != undefined) {
            $('#username').val(username);
        }
    });

    //Get Cookie by key
    hit.getCookie = function (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };


    //On hide Register New User Modal Event
    //noinspection JSJQueryEfficiency
    $("#registerModal").on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $('#registerMessage').empty();
        $('.regModalValue').val("");
        $('#registerForm').show();
    });

    //On show Register New User Modal Event
    //noinspection JSJQueryEfficiency
    $("#registerModal").on('show.bs.modal', function () {
        // e.preventDefault();
        $("#logInModal").modal('hide');
    });

    //Payment Limits BTN click Event
    $(document).on('click', '.limitTypePicker', function () {
        var attributeID = $(this).attr('id').trim();
        var limitInfoForm = $('.limitInfoForm');
        hit.currMaxAmount = {};
        switch (attributeID) {
            case 'montLimAction':
                hit.currMaxAmount = document.getElementById('currMonthMaxLimit').value;
                $('#monthLimitDiv').show();
                limitInfoForm.attr('id', 'month');
                break;
            case 'weekLimAction':
                hit.currMaxAmount = document.getElementById('currWeekMaxLimit').value;
                $('#weekLimitDiv').show();
                limitInfoForm.attr('id', 'week');
                break;
            case 'dayLimAction':
                hit.currMaxAmount = document.getElementById('currDayMaxLimit').value;
                $('#dayLimitDiv').show();
                limitInfoForm.attr('id', 'day');
                break;
            default:
                break;
        }
    });

    //Limit Form submit Event
    $(document).on('submit', '.limitInfoForm', function (e) {
        e.preventDefault();
        var formID = $('.limitInfoForm').attr('id').trim();
        var newMaxAmount, currSpentAmount;
        switch (formID) {
            case 'month':
                newMaxAmount = document.getElementById('currMonthMaxLimit').value;
                currSpentAmount = document.getElementById('currMonthSpentLimit').value;
                hit.editMaxPaymentsLimit("month", "currMonthMaxLimit", currSpentAmount, newMaxAmount);
                break;
            case 'week':
                newMaxAmount = document.getElementById('currWeekMaxLimit').value;
                currSpentAmount = document.getElementById('currWeekSpentLimit').value;
                hit.editMaxPaymentsLimit("week", "currWeekMaxLimit", currSpentAmount, newMaxAmount);
                break;
            case 'day':
                newMaxAmount = document.getElementById('currDayMaxLimit').value;
                currSpentAmount = document.getElementById('currDaySpentLimit').value;
                hit.editMaxPaymentsLimit("day", "currDayMaxLimit", currSpentAmount, newMaxAmount);
                break;
            default:
                break;
        }
    });

    //Change Max Limit
    hit.editMaxPaymentsLimit = function (limitType, maxID, currSpentAmount, newMaxAmount) {
        if (parseFloat(newMaxAmount) >= parseFloat(currSpentAmount)) {
            var monthDataID = document.getElementsByClassName("monthDataID")[0].value;
            $.ajax({
                type: "POST",
                url: hit.hostName + '/Controller/EditMaxLimit',
                dataType: 'json',
                async: true,
                data: {limitType: limitType, newMaxAmount: newMaxAmount, monthDataID: monthDataID},
                success: function (data) {
                    //noinspection JSUnresolvedVariable
                    var editPaymentLimitStatus = data.resultMSG;
                    if (editPaymentLimitStatus == 'success') {
                        hit.currMaxAmount = newMaxAmount;
                        $(maxID).value = newMaxAmount;
                        $('#limitInfoModalMessage').html("");
                        $('#limitInfoModal').modal('hide');
                        hit.ifNeedToShowOverflowModal();
                    }
                    else if (editPaymentLimitStatus == 'fail') {
                        $('#limitInfoModalMessage').html(
                            "<div class='alert alert-danger'><strong>Error ! </strong> Fail To Change Limit Please Try Again !!!</div>");
                    }
                },
                error: function () {
                    $('#limitInfoModalMessage').html(
                        "<div class='alert alert-danger'><strong>Error ! </strong> Fail To Change Limit Please Try Again !!!</div>"
                    );
                }
            });
        }
        else {
            $('#limitInfoModalMessage').html(
                "<div class='alert alert-danger'><strong>Error ! </strong> New Limit Is Lower Then Spent Amount !!!</div>"
            );
        }
    };

    //On hide Limit Info Modal Event
    $("#limitInfoModal").on('hidden.bs.modal', function (e) {
        e.preventDefault();
        var limitInfoForm = $('.limitInfoForm');
        var openedDivType = limitInfoForm.attr('id').trim();
        $('#limitInfoModalMessage').empty();
        switch (openedDivType) {
            case 'month':
                $('#monthLimitDiv').hide();
                limitInfoForm.attr('id', '');
                $('#currMonthMaxLimit').val(hit.currMaxAmount);
                break;
            case 'week':
                $('#weekLimitDiv').hide();
                limitInfoForm.attr('id', '');
                $('#currWeekMaxLimit').val(hit.currMaxAmount);
                break;
            case 'day':
                $('#dayLimitDiv').hide();
                limitInfoForm.attr('id', '');
                $('#currDayMaxLimit').val(hit.currMaxAmount);
                break;
            default:
                break;
        }
    });

    //Add new Payment Form submit Event
    $(document).on('submit', '#newPaymentForm', function (e) {
        e.preventDefault();
        var monthID = document.getElementsByClassName("monthDataID")[0].value;
        hit.newPayment = {};
        hit.newPayment.title = document.getElementById("newPaymentTitle").value;
        hit.newPayment.amount = document.getElementById("newPaymentAmount").value;
        hit.newPayment.description = document.getElementById("newPaymentDescription").value;
        $.ajax({
            type: "POST",
            url: hit.hostName + '/Controller/AddNewPaymentServlet',
            dataType: 'json',
            async: true,
            data: {
                monthID: monthID,
                title: hit.newPayment.title,
                amount: hit.newPayment.amount,
                description: hit.newPayment.description,
                currClientDateTime: Date.now()
            },
            success: function (data) {
                //noinspection JSUnresolvedVariable
                var newPaymentStatus = data.addNewPayment;
                if (newPaymentStatus == 'success') {
                    var table = $("#payTable");
                    hit.addTableRow(data.newPayment, table);
                    //noinspection JSUnresolvedVariable
                    hit.updateSpentLimits(data.newPayment.amount, data.newPayment.creationDate, 'add');
                    $('#newPaymentModal').modal('hide');
                    hit.ifNeedToShowOverflowModal();
                }
                else if (newPaymentStatus == 'fail') {
                    $('#newPaymentModalMessage').load("ReplacesContent.html #failToAddNewPayment");
                }
            },
            error: function () {
                $('#newPaymentModalMessage').load("ReplacesContent.html #failToAddNewPayment");
            }
        });

    });

    //On hide Add New Payment Modal Event
    $("#newPaymentModal").on('hidden.bs.modal', function (e) {
        e.preventDefault();
        $('#newPaymentModalMessage').empty();
        $('#newPaymentTitle').val('');
        $('#newPaymentAmount').val('');
        $('#newPaymentDescription').val('');
    });

    //Update Spent Limits in User Interface
    hit.updateSpentLimits = function (spent, paymentDate, operation) {
        var newValue;
        var isNewDayStart = true;
        var isNewWeekStart = true;
        var editPaymentDate = new Date(paymentDate);
        hit.user.paymentLimitsList.forEach(function (paymentMonth) {
            //noinspection JSUnresolvedVariable
            if (hit.user.currMonthID == paymentMonth.PaymentsMonthData.paymentID) {
                //noinspection JSUnresolvedVariable
                var creationWeekDate = new Date(paymentMonth.PaymentsMonthData.creationWeekDate);
                //noinspection JSUnresolvedVariable
                var creationDayDate = new Date(paymentMonth.PaymentsMonthData.creationDayDate);
                //noinspection JSUnresolvedVariable
                var creationMonthDate = new Date(paymentMonth.PaymentsMonthData.creationMonthDate);
                var creationWeekDateMonthWeekNumber = hit.getMonthWeekNumber(creationWeekDate);
                var firstDayOfCreatedMonth = hit.getFirstDayOfMonth(creationMonthDate);
                if (hit.getMonthWeekNumber(editPaymentDate) != creationWeekDateMonthWeekNumber ||
                    (hit.getMonthWeekNumber(editPaymentDate) == creationWeekDateMonthWeekNumber &&
                    creationWeekDateMonthWeekNumber == 1 &&
                    firstDayOfCreatedMonth == 0)) {
                    isNewWeekStart = false;
                    isNewDayStart = false;
                }
                else if (editPaymentDate.getDay() != creationDayDate.getDay()) {
                    isNewDayStart = false;
                }
                return true;
            }
        });

        if (operation == 'add') {
            newValue = parseFloat(document.getElementById('currMonthSpentLimit').value) + parseFloat(spent);
            $('#currMonthSpentLimit').val(newValue);

            if (isNewWeekStart === true) {
                newValue = parseFloat(document.getElementById('currWeekSpentLimit').value) + parseFloat(spent);
                $('#currWeekSpentLimit').val(newValue);
            }
            if (isNewDayStart === true) {
                newValue = parseFloat(document.getElementById('currDaySpentLimit').value) + parseFloat(spent);
                $('#currDaySpentLimit').val(newValue);
            }
        } else if (operation == 'remove') {
            newValue = parseFloat(document.getElementById('currMonthSpentLimit').value) - parseFloat(spent);
            $('#currMonthSpentLimit').val(newValue);
            if (isNewWeekStart === true) {
                newValue = parseFloat(document.getElementById('currWeekSpentLimit').value) - parseFloat(spent);
                $('#currWeekSpentLimit').val(newValue);
            }
            if (isNewDayStart === true) {
                newValue = parseFloat(document.getElementById('currDaySpentLimit').value) - parseFloat(spent);
                $('#currDaySpentLimit').val(newValue);
            }
        }
    };

    //Get first Day Of Given Month
    hit.getFirstDayOfMonth = function (myDate) {
        var date = new Date(myDate);
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        return firstDay.getDay();
    };

    //Get Week Number In Month From Given Date
    hit.getMonthWeekNumber = function (date) {
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
        return Math.ceil((date.getDate() + firstDay) / 7);
    };


    //On show Chert Modal Event
    //noinspection JSJQueryEfficiency
    $('#chartModal').on('shown.bs.modal', function () {
        hit.drawChart(hit.chartType);
    });

    //On hide Chert Modal Event
    //noinspection JSJQueryEfficiency
    $('#chartModal').on('hidden.bs.modal', function () {
        $('#chartContent').empty();
        $('#chartModalMessage').empty();
        hit.barChart.isPipeChartLoaded = false;
        hit.pipeCharts.isPieChartsLoaded = false;
    });

    //Chart Picker BTN Event
    $(document).on('click', '.chartPicker', function () {
        hit.chartType = $(this).attr('id').trim();
    });

    //Resize That Depends On chart Load
    $(window).resize(function () {
        if ($('#chartModal').hasClass('in')) {
            if (hit.barChart.isPipeChartLoaded === true) {
                hit.drawChart('barChartAction');
            }
            else if (hit.pipeCharts.isPieChartsLoaded === true) {
                hit.drawChart('pieChartAction');
            }
        }
    });

});




