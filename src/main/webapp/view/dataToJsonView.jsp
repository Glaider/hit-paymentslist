<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper" %>
<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="com.hit.model.entities.User" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.hit.model.entities.Payment" %>
<%@ page import="com.hit.model.entities.PaymentsMonthData" %>
<%@page session="false" %>
<jsp:useBean id="requestType" scope="request" type="java.lang.String"/>
<jsp:getProperty name="requestType" property="bytes"/>
<%
    response.setHeader("Access-Control-Allow-Origin", "*");
    ObjectMapper mapper = new ObjectMapper();
    JSONObject jsonObject = new JSONObject();
    String reqType = new String(requestType.getBytes());
    if (reqType.equals("Login")) {
        String loginStatus = (String) request.getAttribute("loginStatus");
        jsonObject.put("loginStatus", loginStatus);
        if (loginStatus.equals("User")) {
            User user = (User) request.getAttribute("userData");
            jsonObject.put("userData", user);
        } else if (loginStatus.equals("Fail")) {
            jsonObject.put("registeredUsersInfo", request.getAttribute("registeredUsersInfo"));
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        }

    } else if (reqType.equals("RegisterNewUser")) {
        String registerStatus = (String) request.getAttribute("register");
        jsonObject.put("register", registerStatus);
        if (registerStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        }
    } else if (reqType.equals("RemovePayment")) {
        String removePaymentStatus = (String) request.getAttribute("deletePayment");
        jsonObject.put("deletePayment", removePaymentStatus);
        if (removePaymentStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        } else {
            long paymentDate = (Long) request.getAttribute("paymentDate");
            jsonObject.put("paymentDate", paymentDate);

        }
    } else if (reqType.equals("EditPayment")) {
        String editPaymentStatus = (String) request.getAttribute("editPayment");
        jsonObject.put("editPayment", editPaymentStatus);
        if (editPaymentStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        } else {
            Payment paymentCreationDateMilliseconds = (Payment) request.getAttribute("payment");
            jsonObject.put("payment", paymentCreationDateMilliseconds);
        }
    } else if (reqType.equals("EditMaxLimits")) {
        String editMaxLimitsStatus = (String) request.getAttribute("resultMSG");
        jsonObject.put("resultMSG", editMaxLimitsStatus);
        if (editMaxLimitsStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        }
    } else if (reqType.equals("DateChecking")) {
        String dateChangeStatus = (String) request.getAttribute("change");
        jsonObject.put("change", dateChangeStatus);
        if (dateChangeStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        } else if (dateChangeStatus.equals("month")) {
            PaymentsMonthData month = (PaymentsMonthData) request.getAttribute("newMonth");
            jsonObject.put("newMonth", month);
        }
    } else if (reqType.equals("CurrentMonthLimits")) {
        String getDataStatus = (String) request.getAttribute("getData");
        jsonObject.put("getData", getDataStatus);
        if (getDataStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        } else {
            float monthLimit = (Float) request.getAttribute("monthLimit");
            float weekLimit = (Float) request.getAttribute("weekLimit");
            float dayLimit = (Float) request.getAttribute("dayLimit");
            float monthLimitSpent = (Float) request.getAttribute("monthLimitSpent");
            float weekLimitSpent = (Float) request.getAttribute("weekLimitSpent");
            float dayLimitSpent = (Float) request.getAttribute("dayLimitSpent");
            jsonObject.put("monthLimit", monthLimit);
            jsonObject.put("weekLimit", weekLimit);
            jsonObject.put("dayLimit", dayLimit);
            jsonObject.put("monthLimitSpent", monthLimitSpent);
            jsonObject.put("weekLimitSpent", weekLimitSpent);
            jsonObject.put("dayLimitSpent", dayLimitSpent);
        }
    } else if (reqType.equals("Logout")) {

    } else if (reqType.equals("AddNewPayment")) {
        String addNewPaymentStatus = (String) request.getAttribute("addNewPayment");
        jsonObject.put("addNewPayment", addNewPaymentStatus);
        if (addNewPaymentStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        } else {
            Payment newPayment = (Payment) request.getAttribute("newPayment");
            jsonObject.put("newPayment", newPayment);
        }
    } else if (reqType.equals("Admin")) {
        String adminDataRequestStatus = (String) request.getAttribute("adminDataRequestStatus");
        jsonObject.put("adminDataRequestStatus", adminDataRequestStatus);
        if (adminDataRequestStatus.equals("fail")) {
            jsonObject.put("errorMessage", request.getAttribute("errorMessage"));
        } else {
            String adminRequestType = request.getParameter("adminDataRequestType");
            if (adminRequestType.equals("getActiveSessions")) {
                jsonObject.put("activeSessions", String.valueOf(application.getAttribute("activesSessions")));
            } else {
                Map<String, String> registeredUsers = (HashMap<String, String>) request.getAttribute("registeredUsersInfo");
                jsonObject.put("registeredUsersInfo", registeredUsers);
            }
        }
    }

    mapper.writeValue(response.getOutputStream(), jsonObject);
    out.flush();
%>