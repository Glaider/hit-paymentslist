package com.hit.model.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

import static org.hibernate.annotations.CascadeType.DELETE;

/**
 * Represents the PaymentsMonthData Entity.
 */
@Entity
@Table(name = "Payment_MonthData")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class PaymentsMonthData {

    /**
     * PaymentsMonthData ID.
     */
    @Id
    @Column(name = "Limit_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long paymentID;

    /**
     * Monthly payments amount limit.
     */
    @Column(name = "Month_Limit")
    private float monthLimit;

    /**
     * Daily payments amount limit.
     */
    @Column(name = "Day_Limit")
    private float dayLimit;

    /**
     * Weekly payments amount limit.
     */
    @Column(name = "Week_Limit")
    private float weekLimit;

    /**
     * Month spent amount.
     */
    @Column(name = "Month_Limit_Spent")
    private float monthLimitSpent;

    /**
     * Week spent amount.
     */
    @Column(name = "Week_Limit_Spent")
    private float weekLimitSpent;

    /**
     * Day spent amount.
     */
    @Column(name = "Day_Limit_Spent")
    private float DayLimitSpent;


    /**
     * Current Month Payments Date in milliseconds.
     */
    @Column(name = "Limit_InitMonthDate")
    private long creationMonthDate;

    /**
     * Current Week Payments Date in milliseconds.
     */
    @Column(name = "Limit_InitWeekDate")
    private long creationWeekDate;

    /**
     * Current Day Payments Date in milliseconds.
     */
    @Column(name = "Limit_InitDayDate")
    private long creationDayDate;

    /**
     * User ID.
     *
     * @see User
     */
    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "User_ID")
    private User user;

    /**
     * Payments List.
     *
     * @see Payment
     */
    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "paymentsMonthData")
    @Cascade(DELETE)
    @Fetch(value = FetchMode.SELECT)
    private List<Payment> paymentList;


    /**
     * Constructs a new PaymentsMonthData.
     */
    public PaymentsMonthData() {
    }

    /**
     * Constructs a new PaymentsMonthData with fields.
     *
     * @param monthLimit
     * @param dayLimit
     * @param weekLimit
     * @param monthLimitSpent
     * @param weekLimitSpent
     * @param dayLimitSpent
     * @param creationMonthDate
     * @param creationWeekDate
     * @param creationDayDate
     * @param user
     * @param paymentList
     */
    public PaymentsMonthData(float monthLimit, float dayLimit, float weekLimit, float monthLimitSpent, float weekLimitSpent, float dayLimitSpent, long creationMonthDate, long creationWeekDate, long creationDayDate, User user, List<Payment> paymentList) {
        setMonthLimit(monthLimit);
        setDayLimit(dayLimit);
        setWeekLimit(weekLimit);
        setMonthLimitSpent(monthLimitSpent);
        setWeekLimitSpent(weekLimitSpent);
        setDayLimitSpent(dayLimitSpent);
        setCreationMonthDate(creationMonthDate);
        setCreationWeekDate(creationWeekDate);
        setCreationDayDate(creationDayDate);
        setUser(user);
        setPaymentList(paymentList);
    }

    /**
     * Getter for property 'monthLimitSpent'.
     *
     * @return Value for property 'monthLimitSpent'.
     */
    public float getMonthLimitSpent() {
        return monthLimitSpent;
    }

    /**
     * Setter for property 'monthLimitSpent'.
     *
     * @param monthLimitSpent Value to set for property 'monthLimitSpent'.
     */
    public void setMonthLimitSpent(float monthLimitSpent) {
        if (monthLimitSpent >= 0) {
            this.monthLimitSpent = monthLimitSpent;
        }
    }

    /**
     * Getter for property 'dayLimitSpent'.
     *
     * @return Value for property 'dayLimitSpent'.
     */
    public float getDayLimitSpent() {
        return DayLimitSpent;
    }

    /**
     * Setter for property 'dayLimitSpent'.
     *
     * @param dayLimitSpent Value to set for property 'dayLimitSpent'.
     */
    public void setDayLimitSpent(float dayLimitSpent) {
        if (dayLimitSpent >= 0) {
            DayLimitSpent = dayLimitSpent;
        }
    }

    /**
     * Getter for property 'weekLimitSpent'.
     *
     * @return Value for property 'weekLimitSpent'.
     */
    public float getWeekLimitSpent() {
        return weekLimitSpent;
    }

    /**
     * Setter for property 'weekLimitSpent'.
     *
     * @param weekLimitSpent Value to set for property 'weekLimitSpent'.
     */
    public void setWeekLimitSpent(float weekLimitSpent) {
        if (weekLimitSpent >= 0) {
            this.weekLimitSpent = weekLimitSpent;
        }
    }

    /**
     * Getter for property 'dayLimit'.
     *
     * @return Value for property 'dayLimit'.
     */
    public float getDayLimit() {
        return dayLimit;
    }

    /**
     * Setter for property 'dayLimit'.
     *
     * @param dayLimit Value to set for property 'dayLimit'.
     */
    public void setDayLimit(float dayLimit) {
        if (dayLimit >= 0) {
            this.dayLimit = dayLimit;
        }
    }

    /**
     * Getter for property 'weekLimit'.
     *
     * @return Value for property 'weekLimit'.
     */
    public float getWeekLimit() {
        return weekLimit;
    }

    /**
     * Setter for property 'weekLimit'.
     *
     * @param weekLimit Value to set for property 'weekLimit'.
     */
    public void setWeekLimit(float weekLimit) {
        if (weekLimit >= 0) {
            this.weekLimit = weekLimit;
        }
    }

    /**
     * Getter for property 'paymentID'.
     *
     * @return Value for property 'paymentID'.
     */
    public long getPaymentID() {
        return paymentID;
    }

    /**
     * Setter for property 'paymentID'.
     *
     * @param paymentID Value to set for property 'paymentID'.
     */
    public void setPaymentID(long paymentID) {
        this.paymentID = paymentID;
    }

    /**
     * Getter for property 'paymentList'.
     *
     * @return Value for property 'paymentList'.
     */
    public List<Payment> getPaymentList() {
        return paymentList;
    }

    /**
     * Setter for property 'paymentList'.
     *
     * @param paymentList Value to set for property 'paymentList'.
     */
    public void setPaymentList(List<Payment> paymentList) {
        if (!paymentList.isEmpty()) {
            this.paymentList = paymentList;
        }
    }

    /**
     * Getter for property 'monthLimit'.
     *
     * @return Value for property 'monthLimit'.
     */
    public float getMonthLimit() {
        return monthLimit;
    }

    /**
     * Setter for property 'monthLimit'.
     *
     * @param monthLimit Value to set for property 'monthLimit'.
     */
    public void setMonthLimit(float monthLimit) {
        if (monthLimit >= 0) {
            this.monthLimit = monthLimit;
        }
    }

    /**
     * Getter for property 'user'.
     *
     * @return Value for property 'user'.
     */
    public User getUser() {
        return user;
    }

    /**
     * Setter for property 'user'.
     *
     * @param user Value to set for property 'user'.
     */
    public void setUser(User user) {
        if (user != null) {
            this.user = user;
        }
    }

    /**
     * Getter for property 'creationDayDate'.
     *
     * @return Value for property 'creationDayDate'.
     */
    public long getCreationDayDate() {
        return creationDayDate;
    }

    /**
     * Setter for property 'creationDayDate'.
     *
     * @param creationDayDate Value to set for property 'creationDayDate'.
     */
    public void setCreationDayDate(long creationDayDate) {
        if (creationDayDate >= 0) {
            this.creationDayDate = creationDayDate;
        }
    }

    /**
     * Getter for property 'creationWeekDate'.
     *
     * @return Value for property 'creationWeekDate'.
     */
    public long getCreationWeekDate() {
        return creationWeekDate;
    }

    /**
     * Setter for property 'creationWeekDate'.
     *
     * @param creationWeekDate Value to set for property 'creationWeekDate'.
     */
    public void setCreationWeekDate(long creationWeekDate) {
        if (creationWeekDate >= 0) {
            this.creationWeekDate = creationWeekDate;
        }
    }

    /**
     * Getter for property 'creationMonthDate'.
     *
     * @return Value for property 'creationMonthDate'.
     */
    public long getCreationMonthDate() {
        return creationMonthDate;
    }

    /**
     * Setter for property 'creationMonthDate'.
     *
     * @param creationMonthDate Value to set for property 'creationMonthDate'.
     */
    public void setCreationMonthDate(long creationMonthDate) {
        if (creationMonthDate >= 0) {
            this.creationMonthDate = creationMonthDate;
        }
    }
}

