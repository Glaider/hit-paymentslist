package com.hit.model.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

import static org.hibernate.annotations.CascadeType.DELETE;

/**
 * Represents the User Entity.
 */
@Entity
@Table(name = "Payment_User")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class User {

    /**
     * User id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "User_ID")
    private long userID;

    /**
     * User Name.
     */
    @Column(name = "User_Name", length = 30, unique = true)
    private String userName;


    /**
     * User Password.
     */
    @Column(name = "User_Password", length = 30)
    private String password;

    /**
     * User Email.
     */
    @Column(name = "User_Email", length = 50, unique = true)
    private String email;


    /**
     * Payments Months List.
     *
     * @see PaymentsMonthData
     */
    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    @Cascade(DELETE)
    @Fetch(value = FetchMode.SELECT)
    private List<PaymentsMonthData> paymentLimitsList;


    /**
     * Constructs a new User.
     */
    public User() {
    }

    /**
     * Constructs new User with fields
     *
     * @param userName
     * @param password
     * @param email
     */
    public User(String userName, String password, String email) {
        setUserName(userName);
        setPassword(password);
        setEmail(email);
    }

    /**
     * Getter for property 'paymentLimitsList'.
     *
     * @return Value for property 'paymentLimitsList'.
     */
    public List<PaymentsMonthData> getPaymentLimitsList() {
        return paymentLimitsList;
    }

    /**
     * Setter for property 'paymentLimitsList'.
     *
     * @param paymentLimitsList Value to set for property 'paymentLimitsList'.
     */
    public void setPaymentLimitsList(List<PaymentsMonthData> paymentLimitsList) {
        if (!paymentLimitsList.isEmpty()) {
            this.paymentLimitsList = paymentLimitsList;
        }
    }

    /**
     * Getter for property 'userID'.
     *
     * @return Value for property 'userID'.
     */
    public long getUserID() {
        return userID;
    }

    /**
     * Setter for property 'userID'.
     *
     * @param userID Value to set for property 'userID'.
     */
    private void setUserID(long userID) {
        this.userID = userID;
    }

    /**
     * Getter for property 'userName'.
     *
     * @return Value for property 'userName'.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter for property 'userName'.
     *
     * @param userName Value to set for property 'userName'.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Getter for property 'password'.
     *
     * @return Value for property 'password'.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for property 'password'.
     *
     * @param password Value to set for property 'password'.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for property 'email'.
     *
     * @return Value for property 'email'.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter for property 'email'.
     *
     * @param email Value to set for property 'email'.
     */
    public void setEmail(String email) {
        this.email = email;
    }


}