package com.hit.model.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;

/**
 * Represents the Payment Entity.
 */
@Entity
@Table(name = "Payment_Payment")

@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class Payment {

    /**
     * Payment ID.
     */
    @Id
    @Column(name = "Payment_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long paymentID;

    /**
     * Payment Header.
     */
    @Column(name = "Payment_Header", length = 100)
    private String header;

    /**
     * Payment Description.
     */
    @Column(name = "Payment_Description", columnDefinition = "TEXT")
    private String description;

    /**
     * Payment Amount.
     */
    @Column(name = "Payment_Amount")
    private float amount;

    /**
     * Payment Creation In Milliseconds.
     */
    @Column(name = "Payment_CreationDate")
    private long creationDate;

    /**
     * Payments Month.
     *
     * @see PaymentsMonthData
     */
    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Limit_ID")
    private PaymentsMonthData paymentsMonthData;

    /**
     * Constructs a new Payment.
     */
    public Payment() {
    }

    /**
     * Constructs a new Payment with fields.
     *
     * @param paymentsMonthData
     * @param header
     * @param description
     * @param amount
     * @param creationDate
     */
    public Payment(PaymentsMonthData paymentsMonthData, String header, String description, float amount, long creationDate) {
        setPaymentsMonthData(paymentsMonthData);
        setHeader(header);
        setDescription(description);
        setAmount(amount);
        setCreationDate(creationDate);
    }

    /**
     * Getter for property 'paymentsMonthData'.
     *
     * @return Value for property 'paymentsMonthData'.
     */
    public PaymentsMonthData getPaymentsMonthData() {
        return paymentsMonthData;
    }

    /**
     * Setter for property 'paymentsMonthData'.
     *
     * @param paymentsMonthData Value to set for property 'paymentsMonthData'.
     */
    public void setPaymentsMonthData(PaymentsMonthData paymentsMonthData) {
        this.paymentsMonthData = paymentsMonthData;
    }

    /**
     * Getter for property 'paymentID'.
     *
     * @return Value for property 'paymentID'.
     */
    public long getPaymentID() {
        return paymentID;
    }

    /**
     * Setter for property 'paymentID'.
     *
     * @param paymentID Value to set for property 'paymentID'.
     */
    private void setPaymentID(long paymentID) {
        this.paymentID = paymentID;
    }

    /**
     * Getter for property 'header'.
     *
     * @return Value for property 'header'.
     */
    public String getHeader() {
        return header;
    }

    /**
     * Setter for property 'header'.
     *
     * @param header Value to set for property 'header'.
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Getter for property 'description'.
     *
     * @return Value for property 'description'.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for property 'description'.
     *
     * @param description Value to set for property 'description'.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for property 'amount'.
     *
     * @return Value for property 'amount'.
     */
    public float getAmount() {
        return amount;
    }

    /**
     * Setter for property 'amount'.
     *
     * @param amount Value to set for property 'amount'.
     */
    public void setAmount(float amount) {
        if (amount >= 0) {
            this.amount = amount;
        }
    }

    /**
     * Getter for property 'creationDate'.
     *
     * @return Value for property 'creationDate'.
     */
    public long getCreationDate() {
        return creationDate;
    }

    /**
     * Setter for property 'creationDate'.
     *
     * @param creationDate Value to set for property 'creationDate'.
     */
    public void setCreationDate(long creationDate) {
        if (creationDate >= 0) {
            this.creationDate = creationDate;
        }
    }
}
