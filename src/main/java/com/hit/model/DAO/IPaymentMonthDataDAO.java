package com.hit.model.DAO;

import com.hit.exception.PaymentProjectException;
import com.hit.model.entities.PaymentsMonthData;

import java.util.List;

/**
 * Interface ,which represents PaymentMonthData Data Access Object.
 */
public interface IPaymentMonthDataDAO {

    /**
     * Method,  which add new PaymentsMonthData to database.
     *
     * @param userEntity
     * @throws PaymentProjectException
     */
    void addPaymentMonthData(PaymentsMonthData userEntity) throws PaymentProjectException;

    /**
     * Method,  which update PaymentsMonthData in database.
     *
     * @param userEntity
     * @throws PaymentProjectException
     */
    void updatePaymentMonthData(PaymentsMonthData userEntity) throws PaymentProjectException;

    /**
     * Method,  which remove PaymentsMonthData from database by ID.
     *
     * @param id
     * @throws PaymentProjectException
     */
    void removePaymentMonthData(long id) throws PaymentProjectException;

    /**
     * Method,  which get all PaymentsMonthData Entities from database.
     *
     * @return List of PaymentsMonthData
     */
    List<PaymentsMonthData> getAllPaymentMonthData() throws PaymentProjectException;

    /**
     * Method,  which get PaymentsMonthData from database by ID.
     *
     * @param id
     * @return PaymentsMonthData
     * @throws PaymentProjectException
     */
    PaymentsMonthData getPaymentMonthDataByIndex(long id) throws PaymentProjectException;

}

