package com.hit.model.DAO;


import com.hit.exception.PaymentProjectException;
import com.hit.model.entities.User;

import java.util.List;

/**
 * Interface ,which represents User Data Access Object.
 */
public interface IUserDAO {

    /**
     * Method,  which add new User to database.
     * @param userEntity
     * @throws PaymentProjectException
     */
    void addUser(User userEntity) throws PaymentProjectException;

    /**
     * Method,  which update User in database.
     * @param userEntity
     * @throws PaymentProjectException
     */
    void updateUser(User userEntity) throws PaymentProjectException;

    /**
     * Method,  which remove User from database by ID.
     * @param id
     * @throws PaymentProjectException
     */
    void removeUser(long id) throws PaymentProjectException;

    /**
     * Method,  which get all Users from database.
     *
     * @return List of Users
     */
    List<User> getAllUsers() throws PaymentProjectException;

    /**
     * Method,  which get User from database by ID.
     * @param id
     * @return User
     * @throws PaymentProjectException
     */
    User getUserByIndex(long id) throws PaymentProjectException;

    /**
     * Method,  which get User from database by username and password.
     * @param userName
     * @param password
     * @return User
     * @throws PaymentProjectException
     */
    User getUserByUsernameAndPassword(String userName, String password) throws PaymentProjectException;
}
