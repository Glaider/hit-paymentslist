package com.hit.model.DAO;

import com.hit.exception.PaymentProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Generic Data Access Object.
 */
abstract class GenericDAOImpl {

    private static final Logger logger = LogManager.getRootLogger();

    /**
     * Generic method, which add new entity to database.
     *
     * @param entity
     * @param <T>
     * @throws PaymentProjectException
     */
    protected static <T> void add(T entity) throws PaymentProjectException {
        Session session;
        Transaction transaction = null;
        logger.debug("Call add method in GenericDAOImpl");
        try {
            session = HibernateFactoryUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
        } catch (HibernateException ex) {
            logger.error("Exception thrown in GenericDAOImpl add method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to add New Entity to DataBase", ex);
        }
    }

    /**
     * Generic method, which update entity in database.
     *
     * @param entity
     * @param <T>
     * @throws PaymentProjectException
     */
    protected static <T> void update(T entity) throws PaymentProjectException {
        Session session;
        Transaction transaction = null;
        logger.debug("Call update method in GenericDAOImpl");
        try {
            session = HibernateFactoryUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            session.update(entity);
            transaction.commit();
        } catch (HibernateException ex) {
            logger.error("Exception thrown in GenericDAOImpl update method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to update Entity in DataBase", ex);
        }
    }

    /**
     * Generic method, which remove entity from database.
     *
     * @param id
     * @param typeParameterClass
     * @param <T>
     * @throws PaymentProjectException
     */
    protected static <T> void remove(long id, Class<T> typeParameterClass) throws PaymentProjectException {
        Session session;
        Transaction transaction = null;
        T entity;
        logger.debug("Call remove method in GenericDAOImpl");
        try {
            session = HibernateFactoryUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            entity = (T) session.load(typeParameterClass, id);
            session.delete(entity);
            transaction.commit();
        } catch (HibernateException ex) {
            logger.error("Exception thrown in GenericDAOImpl remove method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to remove Entity from DataBase", ex);
        }
    }

    /**
     * Generic method, which gets entity from database by ID.
     *
     * @param id
     * @param typeParameterClass
     * @param <T>
     * @return T
     * @throws PaymentProjectException
     */
    protected static <T> T getByIndex(long id, Class<T> typeParameterClass) throws PaymentProjectException {
        Session session;
        Transaction transaction = null;
        T entity;
        logger.debug("Call getByIndex method in GenericDAOImpl");
        try {
            session = HibernateFactoryUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            entity = (T) session.get(typeParameterClass, id);
            transaction.commit();
        } catch (HibernateException ex) {
            logger.error("Exception thrown in GenericDAOImpl getByIndex method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to load Entity from DataBase", ex);
        }
        return entity;
    }

    /**
     * Generic method, which get List of entities from database.
     *
     * @param typeParameterClass
     * @param <T>
     * @return List of T
     * @throws PaymentProjectException
     */
    protected static <T> List<T> getAll(Class<T> typeParameterClass) throws PaymentProjectException {
        Session session;
        Transaction transaction = null;
        List<T> entities;
        logger.debug("Call getAll method in GenericDAOImpl");
        try {
            session = HibernateFactoryUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();

            Criteria criteria = session.createCriteria(typeParameterClass.getName());
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            entities = criteria.list();

            transaction.commit();
        } catch (HibernateException ex) {
            logger.error("Exception thrown in GenericDAOImpl getAll method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to load Entities from DataBase", ex);
        }
        return entities;
    }


}

