package com.hit.model.DAO;

import com.hit.exception.PaymentProjectException;

import java.util.Map;

/**
 * Interface ,which represents Admin Data Access Object.
 */
public interface IAdminDAO {

    /**
     * Method , which get Map of username and emails from database.
     *
     * @return Map
     * @throws PaymentProjectException
     */
    Map<String, String> getUsersRegInfo() throws PaymentProjectException;
}
