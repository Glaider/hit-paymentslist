package com.hit.model.DAO;

import com.hit.exception.PaymentProjectException;
import com.hit.model.entities.Payment;
import com.hit.model.entities.PaymentsMonthData;
import com.hit.model.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Singleton Class, which represents Project Data Access Object.
 */
public class ProjectDAO implements IPaymentDAO, IUserDAO, IPaymentMonthDataDAO, IAdminDAO {
    private static ProjectDAO instance;
    private static final Logger logger = LogManager.getRootLogger();

    private ProjectDAO() {
    }

    /**
     * Method, which get instance of ProjectDAO or creates new one if it doesn't exist.
     *
     * @return ProjectDAO
     */
    public static ProjectDAO getDAO() {
        logger.debug("Call getDAO method in ProjectDAO");
        if (instance == null) {
            instance = new ProjectDAO();
        }
        return instance;
    }

    /**
     * Method, which close SessionFactory.
     */
    public void shutdown() {
        logger.debug("Call shutdown method in ProjectDAO");
        HibernateFactoryUtil.shutdown();
    }

    /**
     * Method, that get SessionFactory
     *
     * @return SessionFactory
     * @throws PaymentProjectException
     */
    public SessionFactory getSessionFactory() throws PaymentProjectException {
        logger.debug("Call getSessionFactory method in ProjectDAO");
        return HibernateFactoryUtil.getSessionFactory();

    }


    //User Dao methods

    /**
     * Method, that add new User to database.
     *
     * @param userEntity
     * @throws PaymentProjectException
     * @see IUserDAO,GenericDAOImpl
     */
    public void addUser(User userEntity) throws PaymentProjectException {
        logger.debug("Call addUser method in ProjectDAO");
        GenericDAOImpl.add(userEntity);
    }

    /**
     * Method,  which update User in database.
     *
     * @param userEntity
     * @throws PaymentProjectException
     * @see IUserDAO,GenericDAOImpl
     */
    public void updateUser(User userEntity) throws PaymentProjectException {
        logger.debug("Call updateUser method in ProjectDAO");
        GenericDAOImpl.update(userEntity);
    }

    /**
     * Method,  which remove User from database by ID.
     *
     * @param id
     * @throws PaymentProjectException
     * @see IUserDAO,GenericDAOImpl
     */
    public void removeUser(long id) throws PaymentProjectException {
        logger.debug("Call removeUser method in ProjectDAO");
        GenericDAOImpl.remove(id, User.class);
    }

    /**
     * Method,  which get all Users from database.
     *
     * @return List of Users
     * @throws PaymentProjectException
     * @see IUserDAO,GenericDAOImpl
     */
    public List<User> getAllUsers() throws PaymentProjectException {
        logger.debug("Call getAllUsers method in ProjectDAO");
        return GenericDAOImpl.getAll(User.class);
    }

    /**
     * Method,  which get User from database by ID.
     *
     * @param id
     * @return User
     * @throws PaymentProjectException
     * @see IUserDAO,GenericDAOImpl
     */
    public User getUserByIndex(long id) throws PaymentProjectException {
        logger.debug("Call getUserByIndex method in ProjectDAO");
        return GenericDAOImpl.getByIndex(id, User.class);
    }

    /**
     * Method,  which get User from database by username and password.
     *
     * @param userName
     * @param password
     * @return User
     * @throws PaymentProjectException
     * @see IUserDAO
     */
    public User getUserByUsernameAndPassword(String userName, String password) throws PaymentProjectException {
        Session session;
        List result;
        Transaction transaction = null;
        User user;
        logger.debug("Call getUserByUsernameAndPassword method in ProjectDAO");
        try {
            session = HibernateFactoryUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from User as c where c.userName = :username and c.password= :password");
            q.setString("username", userName);
            q.setString("password", password);
            result = q.list();
            transaction.commit();
            user = (User) result.get(0);
        } catch (HibernateException ex) {
            logger.error("Exception thrown in ProjectDAO getUserByUsernameAndPassword method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to load Entity from DataBase", ex);
        } catch (IndexOutOfBoundsException ex) {
            logger.error("Exception thrown in ProjectDAO getUserByUsernameAndPassword method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to load Entity from DataBase", ex);
        }
        return user;
    }
    //User Dao methods

    //User Payment Dao methods

    /**
     * Method,  which add new Payment to database.
     *
     * @param paymentEntity
     * @throws PaymentProjectException
     * @see IPaymentDAO,GenericDAOImpl
     */
    public void addPayment(Payment paymentEntity) throws PaymentProjectException {
        logger.debug("Call addPayment method in ProjectDAO");
        GenericDAOImpl.add(paymentEntity);
    }

    /**
     * Method, which update Payment in database.
     *
     * @param paymentEntity
     * @throws PaymentProjectException
     * @see IPaymentDAO,GenericDAOImpl
     */
    public void updatePayment(Payment paymentEntity) throws PaymentProjectException {
        logger.debug("Call updatePayment method in ProjectDAO");
        GenericDAOImpl.update(paymentEntity);
    }

    /**
     * Method, which remove Payment from database by ID.
     *
     * @param id
     * @throws PaymentProjectException
     * @see IPaymentDAO,GenericDAOImpl
     */
    public void removePayment(long id) throws PaymentProjectException {
        logger.debug("Call removePayment method in ProjectDAO");
        GenericDAOImpl.remove(id, Payment.class);
    }

    /**
     * Method, which get Payment from database by ID.
     *
     * @param id
     * @return Payment
     * @throws PaymentProjectException
     * @see IPaymentDAO,GenericDAOImpl
     */
    public Payment getPaymentByIndex(long id) throws PaymentProjectException {
        logger.debug("Call getPaymentByIndex method in ProjectDAO");
        return GenericDAOImpl.getByIndex(id, Payment.class);
    }

    /**
     * Method, which get all Payments from database.
     *
     * @return List of Payments
     * @throws PaymentProjectException
     * @see IPaymentDAO,GenericDAOImpl
     */
    public List<Payment> getAllPayments() throws PaymentProjectException {
        logger.debug("Call getAllPayments method in ProjectDAO");
        return GenericDAOImpl.getAll(Payment.class);
    }
    //User Payment Dao methods

    //User Payment Month Data Dao methods

    /**
     * Method,  which add new PaymentsMonthData to database.
     *
     * @param userEntity
     * @throws PaymentProjectException
     * @see IPaymentMonthDataDAO,GenericDAOImpl
     */
    public void addPaymentMonthData(PaymentsMonthData userEntity) throws PaymentProjectException {
        logger.debug("Call addPaymentMonthData method in ProjectDAO");
        GenericDAOImpl.add(userEntity);
    }

    /**
     * Method,  which update PaymentsMonthData in database.
     *
     * @param userEntity
     * @throws PaymentProjectException
     * @see IPaymentMonthDataDAO,GenericDAOImpl
     */
    public void updatePaymentMonthData(PaymentsMonthData userEntity) throws PaymentProjectException {
        logger.debug("Call updatePaymentMonthData method in ProjectDAO");
        GenericDAOImpl.update(userEntity);
    }

    /**
     * Method,  which remove PaymentsMonthData from database by ID.
     *
     * @param id
     * @throws PaymentProjectException
     * @see IPaymentMonthDataDAO,GenericDAOImpl
     */
    public void removePaymentMonthData(long id) throws PaymentProjectException {
        logger.debug("Call removePaymentMonthData method in ProjectDAO");
        GenericDAOImpl.remove(id, PaymentsMonthData.class);
    }

    /**
     * Method,  which get all PaymentsMonthData Entities from database.
     *
     * @return List of PaymentsMonthData
     * @throws PaymentProjectException
     * @see IPaymentMonthDataDAO,GenericDAOImpl
     */
    public List<PaymentsMonthData> getAllPaymentMonthData() throws PaymentProjectException {
        logger.debug("Call getAllPaymentMonthData method in ProjectDAO");
        return GenericDAOImpl.getAll(PaymentsMonthData.class);
    }

    /**
     * Method,  which get PaymentsMonthData from database by ID.
     *
     * @param id
     * @return PaymentsMonthData
     * @throws PaymentProjectException
     * @see IPaymentMonthDataDAO,GenericDAOImpl
     */
    public PaymentsMonthData getPaymentMonthDataByIndex(long id) throws PaymentProjectException {
        logger.debug("Call getPaymentMonthDataByIndex method in ProjectDAO");
        return GenericDAOImpl.getByIndex(id, PaymentsMonthData.class);
    }

    /**
     * Method , which get Map of username and emails from database.
     *
     * @return Map
     * @throws PaymentProjectException
     */
    @Override
    public Map<String, String> getUsersRegInfo() throws PaymentProjectException {
        Map<String, String> registeredUsers = new HashMap<String, String>();
        Session session;
        Transaction transaction = null;
        logger.debug("Call getUsersRegInfo method in ProjectDAO");
        try {
            session = HibernateFactoryUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("select c.userName , c.email from User c");

            List<Object[]> users = (List<Object[]>) q.list();
            for (Object[] user : users) {
                String username = (String) user[0];
                String email = (String) user[1];
                registeredUsers.put(username, email);
            }
            transaction.commit();
        } catch (HibernateException ex) {
            logger.error("Exception thrown in ProjectDAO getUsersRegInfo method ,Error Description:" + ex.getMessage());
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new PaymentProjectException("Filed to load Entity from DataBase", ex);
        }
        return registeredUsers;
    }
    //User Payment Month Data Dao methods

}