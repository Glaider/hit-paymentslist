package com.hit.model.DAO;

import com.hit.exception.PaymentProjectException;
import com.hit.model.entities.Payment;

import java.util.List;

/**
 * Interface ,which represents Payment Data Access Object.
 */
public interface IPaymentDAO {

    /**
     * Method,  which add new Payment to database.
     *
     * @param paymentEntity
     * @throws PaymentProjectException
     */
    void addPayment(Payment paymentEntity) throws PaymentProjectException;

    /**
     * Method, which update Payment in database.
     *
     * @param paymentEntity
     * @throws PaymentProjectException
     */
    void updatePayment(Payment paymentEntity) throws PaymentProjectException;

    /**
     * Method, which remove Payment from database by ID.
     *
     * @param id
     * @throws PaymentProjectException
     */
    void removePayment(long id) throws PaymentProjectException;

    /**
     * Method, which get Payment from database by ID.
     *
     * @param id
     * @return Payment
     * @throws PaymentProjectException
     */
    Payment getPaymentByIndex(long id) throws PaymentProjectException;

    /**
     * Method, which get all Payments from database.
     *
     * @return List of Payments
     */
    List<Payment> getAllPayments() throws PaymentProjectException;
}
