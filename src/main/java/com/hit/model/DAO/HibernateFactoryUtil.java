package com.hit.model.DAO;

import com.hit.exception.PaymentProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Singleton Class, which Represents Hibernate Session Factory
 */
abstract class HibernateFactoryUtil {

    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;
    private static final Logger logger = LogManager.getRootLogger();

    private static SessionFactory buildSessionFactory() throws PaymentProjectException {
        logger.debug("Call buildSessionFactory method in HibernateFactoryUtil");
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Exception e) {
            logger.error("Exception thrown in HibernateFactoryUtil buildSessionFactory method ,Error Description:" + e.getMessage());
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
            throw new PaymentProjectException("Failed to build the SessionFactory", e);
        }
        return sessionFactory;
    }

    /**
     * Method, which returns session factory or creates new one if it doesn't exist.
     *
     * @return SessionFactory
     */
    static SessionFactory getSessionFactory() throws PaymentProjectException {
        logger.debug("Call getSessionFactory method in HibernateFactoryUtil");
        if (sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }

    /**
     * Method, which close SessionFactory.
     */
    static void shutdown() {
        logger.debug("Call shutdown method in HibernateFactoryUtil");
        // Close caches and connection pools
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}


