package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;
import com.hit.model.entities.Payment;
import com.hit.model.entities.PaymentsMonthData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

/**
 * Servlet , which edit current payment by using ProjectDAO.
 */
@WebServlet(name = "EditPaymentServlet")
public class EditPaymentServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in EditPaymentServlet");
        String paymentID = request.getParameter("paymentID");
        String paymentTitle = request.getParameter("paymentTitle");
        float paymentAmount = Float.parseFloat(request.getParameter("paymentAmount"));
        String paymentDescription = request.getParameter("paymentDescription");
        int timeZoneOffset = Integer.parseInt(request.getParameter("timezone"));
        ProjectDAO dao = ProjectDAO.getDAO();
        Payment editablePayment;
        PaymentsMonthData editablePaymentMonthData;
        boolean isSameWeek;
        boolean isSameDay;
        request.setAttribute("requestType", "EditPayment");
        try {
            editablePayment = dao.getPaymentByIndex(Long.parseLong(paymentID));
            editablePaymentMonthData = editablePayment.getPaymentsMonthData();
            editablePaymentMonthData.setMonthLimitSpent(editablePaymentMonthData.getMonthLimitSpent() - editablePayment.getAmount() + paymentAmount);

            Calendar monthWeekDateWithOffset = Calendar.getInstance();
            monthWeekDateWithOffset.setTimeInMillis(editablePaymentMonthData.getCreationWeekDate());
            monthWeekDateWithOffset.add(Calendar.MINUTE, timeZoneOffset * (-1));

            Calendar monthDayDateWithOffset = Calendar.getInstance();
            monthDayDateWithOffset.setTimeInMillis(editablePaymentMonthData.getCreationDayDate());
            monthDayDateWithOffset.add(Calendar.MINUTE, timeZoneOffset * (-1));

            Calendar paymentDateWithOffset = Calendar.getInstance();
            paymentDateWithOffset.setTimeInMillis(editablePayment.getCreationDate());
            paymentDateWithOffset.add(Calendar.MINUTE, timeZoneOffset * (-1));

            isSameWeek = ControllerUtils.isSameWeek(monthWeekDateWithOffset.getTimeInMillis(), paymentDateWithOffset.getTimeInMillis());
            isSameDay = ControllerUtils.isSameDay(monthDayDateWithOffset.getTimeInMillis(), paymentDateWithOffset.getTimeInMillis());
            if (isSameWeek) {
                editablePaymentMonthData.setWeekLimitSpent(editablePaymentMonthData.getWeekLimitSpent() - editablePayment.getAmount() + paymentAmount);
                if (isSameDay) {
                    editablePaymentMonthData.setDayLimitSpent(editablePaymentMonthData.getDayLimitSpent() - editablePayment.getAmount() + paymentAmount);
                }
            } else if (isSameDay) {
                editablePaymentMonthData.setDayLimitSpent(editablePaymentMonthData.getDayLimitSpent() - editablePayment.getAmount() + paymentAmount);
            }
            editablePayment.setHeader(paymentTitle);
            editablePayment.setAmount(paymentAmount);
            editablePayment.setDescription(paymentDescription);
            dao.updatePayment(editablePayment);
            dao.updatePaymentMonthData(editablePaymentMonthData);
            request.setAttribute("editPayment", "success");
            request.setAttribute("payment", editablePayment);
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in EditPaymentServlet ,Error Description:" + e.getMessage());
            request.setAttribute("editPayment", "fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


}
