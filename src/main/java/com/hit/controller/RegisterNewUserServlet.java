package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;
import com.hit.model.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet , which Registering new User with username , password and Email by using ProjectDAO.
 */
@WebServlet(name = "RegisterNewUserServlet")
public class RegisterNewUserServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in RegisterNewUserServlet");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        ProjectDAO dao = ProjectDAO.getDAO();
        User newUser = new User(username, password, email);
        request.setAttribute("requestType", "RegisterNewUser");
        try {
            dao.addUser(newUser);
            request.setAttribute("register", "success");
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in RegisterNewUserServlet ,Error Description:" + e.getMessage());
            request.setAttribute("register", "fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);

        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


}
