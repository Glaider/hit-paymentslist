package com.hit.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;
/**
 * Class, which contains controller helper methods.
 */
public class ControllerUtils {

    private static final Logger logger = LogManager.getRootLogger();

    /**
     * Getter for property 'utilLogger'.
     *
     * @return Value for property 'utilLogger'.
     */
    public static Logger getUtilLogger()
    {
        return logger;
    }

    /**
     * Method receives two dates which  represented in milliseconds and check if it is same day.
     *
     * @param editablePaymentMonthDataDayDate
     * @param paymentDate
     * @return boolean
     */
    public static boolean isSameDay(long editablePaymentMonthDataDayDate, long paymentDate) {
        boolean isSameDay = true;
        ControllerUtils.getUtilLogger().debug("Call isSameDay method in ControllerUtils");
        Calendar editablePaymentMonthDataWeekCalendar = Calendar.getInstance();
        Calendar paymentCalendar = Calendar.getInstance();
        editablePaymentMonthDataWeekCalendar.setTimeInMillis(editablePaymentMonthDataDayDate);
        paymentCalendar.setTimeInMillis(paymentDate);
        if (editablePaymentMonthDataWeekCalendar.get(Calendar.DAY_OF_MONTH) != paymentCalendar.get(Calendar.DAY_OF_MONTH)) {
            isSameDay = false;
        }
        return isSameDay;
    }

    /**
     * Method receives two dates which  represented in milliseconds and check if it is same week.
     *
     * @param editablePaymentMonthDataWeekDate
     * @param paymentDate
     * @return boolean
     */
    public static boolean isSameWeek(long editablePaymentMonthDataWeekDate, long paymentDate) {
        boolean isSameWeek = true;
        ControllerUtils.getUtilLogger().debug("Call isSameWeek method in ControllerUtils");
        if (getCurrentWeekNumber(editablePaymentMonthDataWeekDate) != getCurrentWeekNumber(paymentDate)) {
            isSameWeek = false;
        }
        return isSameWeek;
    }

    /**
     * Method receives date which represented in milliseconds and return week number.
     *
     * @param date
     * @return int
     */
    public static int getCurrentWeekNumber(long date) {
        ControllerUtils.getUtilLogger().debug("Call getCurrentWeekNumber method in ControllerUtils");
        Calendar calendar = Calendar.getInstance();
        calendar.setMinimalDaysInFirstWeek(1);
        calendar.setTimeInMillis(date);
        return calendar.get(Calendar.WEEK_OF_MONTH);
    }


}
