package com.hit.controller;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter, which filtering all requests to the server.
 */
public class ControllerFilter implements Filter {


    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        ControllerUtils.getUtilLogger().debug("Call doFilter method in ControllerFilter");
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        String homeURL = httpServletRequest.getContextPath() + "/index.html";
        String URI = httpServletRequest.getRequestURI();

        // If request will not be recognised then it will be redirected back to the index.html.
        if (URI.startsWith("/Controller/") || URI.startsWith("/dataToJsonView.jsp")) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            httpServletResponse.sendRedirect(homeURL);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {

    }
}
