package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;
import com.hit.model.entities.Payment;
import com.hit.model.entities.PaymentsMonthData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

/**
 * Servlet , which adding new payment to the current month by received monthID by using ProjectDAO.
 */
@WebServlet(name = "AddNewPaymentServlet")
public class AddNewPaymentServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in AddNewPaymentServlet");
        ProjectDAO dao = ProjectDAO.getDAO();
        long monthID = Long.parseLong(request.getParameter("monthID"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        long amount = Long.parseLong(request.getParameter("amount"));
        Calendar currClientDateTime = Calendar.getInstance();
        currClientDateTime.setTimeInMillis(Long.parseLong(request.getParameter("currClientDateTime")));
        request.setAttribute("requestType", "AddNewPayment");
        try {
            PaymentsMonthData currMonth = dao.getPaymentMonthDataByIndex(monthID);
            Payment newPayment = new Payment();
            newPayment.setCreationDate(currClientDateTime.getTimeInMillis());
            newPayment.setPaymentsMonthData(currMonth);
            newPayment.setHeader(title);
            newPayment.setAmount(amount);
            newPayment.setDescription(description);
            currMonth.setDayLimitSpent(currMonth.getDayLimitSpent() + amount);
            currMonth.setWeekLimitSpent(currMonth.getWeekLimitSpent() + amount);
            currMonth.setMonthLimitSpent(currMonth.getMonthLimitSpent() + amount);
            dao.addPayment(newPayment);
            dao.updatePaymentMonthData(currMonth);
            request.setAttribute("addNewPayment", "success");
            request.setAttribute("newPayment", newPayment);
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in AddNewPaymentServlet ,Error Description:" + e.getMessage());
            request.setAttribute("addNewPayment", "fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);
        }

    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


}
