package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;
import com.hit.model.entities.Payment;
import com.hit.model.entities.PaymentsMonthData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

/**
 * Servlet , which removes payment from DB by received paymentID by using ProjectDAO.
 */
@WebServlet(name = "RemovePaymentServlet")
public class RemovePaymentServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in RemovePaymentServlet");
        ProjectDAO dao = ProjectDAO.getDAO();
        long paymentID = Long.parseLong(request.getParameter("paymentID"));
        boolean isSameWeek;
        boolean isSameDay;
        int timeZoneOffset = Integer.parseInt(request.getParameter("timezone"));
        request.setAttribute("requestType", "RemovePayment");
        try {
            Payment payment = dao.getPaymentByIndex(paymentID);
            long paymentDate = payment.getCreationDate();
            PaymentsMonthData month = dao.getPaymentMonthDataByIndex(payment.getPaymentsMonthData().getPaymentID());
            month.setMonthLimitSpent(month.getMonthLimitSpent() - payment.getAmount());

            Calendar monthWeekDateWithOffset = Calendar.getInstance();
            monthWeekDateWithOffset.setTimeInMillis(month.getCreationWeekDate());
            monthWeekDateWithOffset.add(Calendar.MINUTE, timeZoneOffset * (-1));

            Calendar monthDayDateWithOffset = Calendar.getInstance();
            monthDayDateWithOffset.setTimeInMillis(month.getCreationDayDate());
            monthDayDateWithOffset.add(Calendar.MINUTE, timeZoneOffset * (-1));

            Calendar paymentDateWithOffset = Calendar.getInstance();
            paymentDateWithOffset.setTimeInMillis(payment.getCreationDate());
            paymentDateWithOffset.add(Calendar.MINUTE, timeZoneOffset * (-1));

            isSameWeek = ControllerUtils.isSameWeek(monthWeekDateWithOffset.getTimeInMillis(), paymentDateWithOffset.getTimeInMillis());
            isSameDay = ControllerUtils.isSameDay(monthDayDateWithOffset.getTimeInMillis(), paymentDateWithOffset.getTimeInMillis());
            if (isSameWeek) {
                month.setWeekLimitSpent(month.getWeekLimitSpent() - payment.getAmount());
                if (isSameDay) {
                    month.setDayLimitSpent(month.getDayLimitSpent() - payment.getAmount());
                }
            } else if (isSameDay) {
                month.setDayLimitSpent(month.getDayLimitSpent() - payment.getAmount());
            }
            dao.updatePaymentMonthData(month);
            dao.removePayment(paymentID);
            request.setAttribute("deletePayment", "success");
            request.setAttribute("paymentDate", paymentDate);
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in RemovePaymentServlet ,Error Description:" + e.getMessage());
            request.setAttribute("deletePayment", "fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
