package com.hit.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        ControllerUtils.getUtilLogger().debug("Call sessionCreated method in SessionListener");
        HttpSession session = httpSessionEvent.getSession();
        ServletContext application = session.getServletContext();
        Object sessionObj = application.getAttribute("activesSessions");
        if (sessionObj == null) {
            application.setAttribute("activesSessions", 1);
        } else {
            int count = Integer.parseInt(String.valueOf(application.getAttribute("activesSessions")));
            application.setAttribute("activesSessions", (count + 1));
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        ControllerUtils.getUtilLogger().debug("Call sessionDestroyed method in SessionListener");
        HttpSession session = httpSessionEvent.getSession();
        ServletContext application = session.getServletContext();
        int count = Integer.parseInt(String.valueOf(application.getAttribute("activesSessions")));
        application.setAttribute("activesSessions", (count - 1));
    }
}
