package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;
import com.hit.model.entities.PaymentsMonthData;
import com.hit.model.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.*;

/**
 * Servlet , which gets from DB requested user/admin by using ProjectDAO.
 */
@WebServlet(name = "LogInServlet")
public class LogInServlet extends HttpServlet {

    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in LogInServlet");
        String password = request.getParameter("password"), username = request.getParameter("username");
        ProjectDAO dao = ProjectDAO.getDAO();
        long millisClientDate = Long.parseLong(request.getParameter("currDateTime"));
        int timeZoneOffset = Integer.parseInt(request.getParameter("timezone"));
        Calendar currClientDateTime = Calendar.getInstance();
        currClientDateTime.setTimeInMillis(millisClientDate);
        currClientDateTime.add(Calendar.MINUTE, timeZoneOffset*(-1));
        boolean isRememberUser = Boolean.parseBoolean(request.getParameter("isRememberUser"));
        User user;
        request.setAttribute("requestType", "Login");
        try {
            user = dao.getUserByUsernameAndPassword(username, password);
            if (username.equals("Admin")) {
                request.setAttribute("loginStatus", "Admin");
            } else {
                List<PaymentsMonthData> paymentsLimitsList = user.getPaymentLimitsList();
                if (paymentsLimitsList.size() == 0) {
                    PaymentsMonthData paymentsMonthData = new PaymentsMonthData();
                    paymentsMonthData.setUser(user);
                    paymentsMonthData.setCreationDayDate(millisClientDate);
                    paymentsMonthData.setCreationWeekDate(millisClientDate);
                    paymentsMonthData.setCreationMonthDate(millisClientDate);
                    dao.addPaymentMonthData(paymentsMonthData);
                    user = dao.getUserByIndex(user.getUserID());
                } else {
                    PaymentsMonthData lastMonth = paymentsLimitsList.get(paymentsLimitsList.size() - 1);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(lastMonth.getCreationMonthDate());
                    calendar.add(Calendar.MINUTE, timeZoneOffset*(-1));
                    if (calendar.get(Calendar.MONTH) != currClientDateTime.get(Calendar.MONTH) ||
                            calendar.get(Calendar.YEAR) != currClientDateTime.get(Calendar.YEAR)
                            ) {
                        dao.addPaymentMonthData(createCurrentMonth(user, lastMonth, calendar,currClientDateTime, millisClientDate));
                        user = dao.getUserByIndex(user.getUserID());
                    } else {
                        boolean isNewWeekOrDay = false;
                        /////IF Is Current Week
                        Calendar lastMonthWeekDate = Calendar.getInstance();
                        lastMonthWeekDate.setTimeInMillis(lastMonth.getCreationWeekDate());
                        lastMonthWeekDate.add(Calendar.MINUTE,timeZoneOffset*(-1));
                        if(!ControllerUtils.isSameWeek(lastMonthWeekDate.getTimeInMillis(),currClientDateTime.getTimeInMillis())){
                            lastMonth.setWeekLimit(0);
                            lastMonth.setWeekLimitSpent(0);
                            lastMonth.setCreationWeekDate(millisClientDate);
                            isNewWeekOrDay = true;
                        }/////If Is Current Day
                        calendar.setTimeInMillis(lastMonth.getCreationDayDate());
                        calendar.add(Calendar.MINUTE, timeZoneOffset*(-1));
                        if (calendar.get(Calendar.DAY_OF_MONTH) != currClientDateTime.get(Calendar.DAY_OF_MONTH)) {
                            lastMonth.setDayLimit(0);
                            lastMonth.setDayLimitSpent(0);
                            lastMonth.setCreationDayDate(millisClientDate);
                            isNewWeekOrDay = true;
                        }
                        if (isNewWeekOrDay) {
                            dao.updatePaymentMonthData(lastMonth);
                            user = dao.getUserByIndex(user.getUserID());
                        }
                    }
                }
                request.setAttribute("loginStatus", "User");
                request.setAttribute("userData", user);
                fetchUsernameCookie(isRememberUser, username, response, request);
            }
            request.getSession(true);
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in LogInServlet ,Error Description:" + e.getMessage());
            request.setAttribute("loginStatus", "Fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);
        }

    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init() throws ServletException {
        super.init();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        ProjectDAO dao = ProjectDAO.getDAO();
        String defaultAdminPass = getInitParameter("defaultAdminPassword");
        User adminUser = new User("Admin", defaultAdminPass, "Admin@Admin");
        ControllerUtils.getUtilLogger().debug("Call init method in LogInServlet");
        try {
            dao.addUser(adminUser);

        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in LogInServlet init ,Error Description:" + e.getMessage());
            adminUser = null;
        }
    }


    /**
     * Method , which initialise new month to requested user.
     *
     * @param user
     * @param pastMonthData
     * @param lastMonthDate
     * @param currClientDate
     * @param millisClientDate
     * @return PaymentsMonthData
     */
    private PaymentsMonthData createCurrentMonth(User user, PaymentsMonthData pastMonthData, Calendar lastMonthDate, Calendar currClientDate, long millisClientDate) {
        PaymentsMonthData currentMonthData = new PaymentsMonthData();
        boolean isWeekFromPastMonth = false;
        ControllerUtils.getUtilLogger().debug("Call createCurrentMonth method in LogInServlet");
        currentMonthData.setCreationDayDate(millisClientDate);
        currentMonthData.setCreationMonthDate(millisClientDate);
        int maxWeekNumber = lastMonthDate.getActualMaximum(Calendar.WEEK_OF_MONTH);
        if ((currClientDate.get(Calendar.MONTH) - lastMonthDate.get(Calendar.MONTH)) == 1 && (currClientDate.get(Calendar.YEAR) - lastMonthDate.get(Calendar.YEAR)) == 0) {
            if (ControllerUtils.getCurrentWeekNumber(currClientDate.getTimeInMillis()) == 1 && currClientDate.get(Calendar.DAY_OF_WEEK) != 1
                    && ControllerUtils.getCurrentWeekNumber(pastMonthData.getCreationWeekDate()) == maxWeekNumber) {
                currentMonthData.setWeekLimit(pastMonthData.getWeekLimit());
                currentMonthData.setWeekLimitSpent(pastMonthData.getWeekLimitSpent());
                isWeekFromPastMonth = true;
            }
        }
        if (!isWeekFromPastMonth) {
            currentMonthData.setCreationWeekDate(millisClientDate);
        }
        currentMonthData.setUser(user);
        return currentMonthData;
    }


    /**
     * Method , which initialise user Cookie with userName
     *
     * @param isRememberUser
     * @param userName
     * @param response
     * @param request
     */
    private void fetchUsernameCookie(boolean isRememberUser, String userName, HttpServletResponse response, HttpServletRequest request) {
        ControllerUtils.getUtilLogger().debug("Call fetchUsernameCookie method in LogInServlet");
        if (isRememberUser) {
            Cookie rememberMeCookie = new Cookie("userName", userName);
            if (rememberMeCookie != null) {
                rememberMeCookie.setMaxAge(60 * 60 * 24 * 7);
                rememberMeCookie.setPath("/");
                response.addCookie(rememberMeCookie);
            }
        } else {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("userName")) {
                        cookie.setValue("");
                        cookie.setPath("/");
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                    }
                }
            }
        }

    }
}
