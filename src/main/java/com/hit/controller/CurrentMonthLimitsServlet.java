package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;
import com.hit.model.entities.PaymentsMonthData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet , which return current month payment limits by received paymentID by using ProjectDAO.
 */
@WebServlet(name = "CurrentMonthLimitsServlet")
public class CurrentMonthLimitsServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in CurrentMonthLimitsServlet");
        ProjectDAO dao = ProjectDAO.getDAO();
        long monthID = Long.parseLong(request.getParameter("monthID"));
        request.setAttribute("requestType", "CurrentMonthLimits");
        try {
            PaymentsMonthData month = dao.getPaymentMonthDataByIndex(monthID);
            request.setAttribute("getData", "success");
            request.setAttribute("monthLimit", month.getMonthLimit());
            request.setAttribute("weekLimit", month.getWeekLimit());
            request.setAttribute("dayLimit", month.getDayLimit());
            request.setAttribute("monthLimitSpent", month.getMonthLimitSpent());
            request.setAttribute("weekLimitSpent", month.getWeekLimitSpent());
            request.setAttribute("dayLimitSpent", month.getDayLimitSpent());
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in CurrentMonthLimitsServlet ,Error Description:" + e.getMessage());
            request.setAttribute("getData", "fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
