package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

/**
 * Servlet , which getting requested admin data from DB by using ProjectDAO or
 * getting number of active sessions.
 */
@WebServlet(name = "AdminServlet")
public class AdminServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in AdminServlet");
        String adminRequestType = request.getParameter("adminDataRequestType");
        request.setAttribute("requestType", "Admin");
        ProjectDAO dao = ProjectDAO.getDAO();
        try {
            if (adminRequestType.equals("getRegisteredUsers")) {
                Map<String, String> registeredUsers;
                registeredUsers = dao.getUsersRegInfo();
                request.setAttribute("adminDataRequestStatus", "success");
                request.setAttribute("registeredUsersInfo", registeredUsers);

            } else if (adminRequestType.equals("getActiveSessions")) {
                request.setAttribute("adminDataRequestStatus", "success");
            } else {
                request.setAttribute("adminDataRequestStatus", "fail");
                request.setAttribute("errorMessage", "wrong data request");
            }
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in AdminServlet ,Error Description:" + e.getMessage());
            request.setAttribute("adminDataRequestStatus", "fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
