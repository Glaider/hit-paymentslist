package com.hit.controller;

import com.hit.exception.PaymentProjectException;
import com.hit.model.DAO.ProjectDAO;
import com.hit.model.entities.PaymentsMonthData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet , which change max payment day , week or month limit to current month by using ProjectDAO.
 */
@WebServlet(name = "EditMaxLimitServlet")
public class EditMaxLimitServlet extends HttpServlet {
    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        ControllerUtils.getUtilLogger().debug("Call doPost method in EditMaxLimitServlet");
        String limitType = request.getParameter("limitType");
        long monthDataID = Long.parseLong(request.getParameter("monthDataID"));
        long newMaxAmount = Long.parseLong(request.getParameter("newMaxAmount"));

        ProjectDAO dao = ProjectDAO.getDAO();
        request.setAttribute("requestType", "EditMaxLimits");
        try {
            PaymentsMonthData paymentsMonthData = dao.getPaymentMonthDataByIndex(monthDataID);
            if (limitType.equals("month")) {
                paymentsMonthData.setMonthLimit(newMaxAmount);
            } else if (limitType.equals("week")) {
                paymentsMonthData.setWeekLimit(newMaxAmount);
            } else if (limitType.equals("day")) {
                paymentsMonthData.setDayLimit(newMaxAmount);
            }
            dao.updatePaymentMonthData(paymentsMonthData);
            request.setAttribute("resultMSG", "success");
        } catch (PaymentProjectException e) {
            ControllerUtils.getUtilLogger().error("Exception thrown in EditMaxLimitServlet ,Error Description:" + e.getMessage());
            request.setAttribute("resultMSG", "fail");
            request.setAttribute("errorMessage", e.getMessage());
        } finally {
            request.getRequestDispatcher("/view/dataToJsonView.jsp").forward(request, response);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
