package com.hit.exception;

/**
 * The Class PaymentProjectException extends Exception class. Thrown for all Program issues that can occur.
 */
public class PaymentProjectException extends Exception {

    public PaymentProjectException(String message) {
        super(message);
    }

    public PaymentProjectException(String message, Throwable cause) {
        super(message, cause);
    }


}

